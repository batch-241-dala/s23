// Create an object using object literals

let trainer = {};

// Properties
trainer.name = 'Ash Ketchum';
trainer.age = '10';
trainer.pokemon = ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'];
trainer.friends = {
	kanto: ['Brock', 'Misty'],
	hoenn: ['May', 'Max']
}


// Methods

trainer.talk = function() {
	console.log('Pikachu, I choose you!');
}

trainer.talk();

// Check if all props are added
console.log(trainer);

// Access object properties using dot notation
console.log('Result of dot notation:');
console.log(trainer.name);

// Access object properties using square brackets
console.log('Result of square bracket notation');
console.log(trainer['pokemon']);

// Access the trainer "talk" method
console.log('Result of talk method:');
trainer.talk();

// Create a constructor function for creating a pokemon
function Pokemon(name, level) {

	// Properties
	this.name = name;
	this.level = level;
	this.health	= 2 * level;
	this.attack = level;

	// Methods
	this.tackle = function(target) {
		console.log(this.name + " tackled " + target.name)
		
		// Reduces the target object's health property by subtracting and reassigning its value to the pokemon's attack
		target.health -= this.attack;

		console.log(target.name + "'s health is now reduced to " + target.health)

		// If the health of the target is less than or equal to 0, invoke faint function
		if (target.health <= 0) {
			target.faint()
		}
	};

	this.faint = function() {
		console.log(this.name + " fainted");
	}

}

// Instantiate a new pokemon
let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);

// 
let raichu = new Pokemon("Raichu", 15);
console.log(raichu);

let missingno = new Pokemon("Missingno", 18);
console.log(missingno);

raichu.tackle(pikachu);
console.log(pikachu);

// 
missingno.tackle(pikachu);
console.log(pikachu);
